package com.example.android.animalsquiz;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * This app displays the number of points received in the quiz.
 */
public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void submitAnswers(View view) {

        RadioButton yes1_radiobutton = findViewById(R.id.yes1_radiobutton);
        boolean goodAnswer1 = yes1_radiobutton.isChecked();

        EditText dolphinField = findViewById(R.id.dolphin_field);
        String dolphin = dolphinField.getText().toString();
        boolean dolphinResult = dolphin.toLowerCase().trim().equals("dolphin");

        RadioButton eighty_radiobutton = findViewById(R.id.eighty_radiobutton);
        boolean goodAnswer3 = eighty_radiobutton.isChecked();

        RadioButton yes4_radiobutton = findViewById(R.id.yes4_radiobutton);
        boolean goodAnswer4 = yes4_radiobutton.isChecked();

        EditText giraffeField = findViewById(R.id.giraffe_field);
        String giraffe = giraffeField.getText().toString();
        boolean giraffeResult = giraffe.toLowerCase().trim().equals("giraffe");

        CheckBox memoryCheckBox = findViewById(R.id.memory_checkbox);
        boolean trueMemory = memoryCheckBox.isChecked();

        CheckBox birthCheckBox = findViewById(R.id.birth_checkbox);
        boolean trueBirth = birthCheckBox.isChecked();

        CheckBox mammalCheckBox = findViewById(R.id.notMammal_checkbox);
        boolean falseMammal = mammalCheckBox.isChecked();

        int score = calculatePoints(goodAnswer1, dolphinResult, goodAnswer3, goodAnswer4, giraffeResult, trueMemory, trueBirth, falseMammal);
        String pointsMessage = createPointsSummary(score);
        displayToastMessage(pointsMessage);
        String scoreMessage = createScoreMessage(goodAnswer1, dolphinResult, goodAnswer3, goodAnswer4, giraffeResult, trueMemory, trueBirth, falseMammal);


        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, "AnimalsQuiz " + pointsMessage);
        intent.putExtra(Intent.EXTRA_TEXT, scoreMessage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }


    }

    private void displayToastMessage(String pointsMessage) {
        Toast.makeText(MainActivity.this, pointsMessage,
                Toast.LENGTH_LONG).show();
    }

    private int calculatePoints(boolean q1, boolean q2, boolean q3, boolean q4, boolean q5, boolean q6a, boolean q6b, boolean q6c) {
        int startPoints = 0;

        if (q1) {
            startPoints++;
        }
        if (q2) {
            startPoints++;
        }
        if (q3) {
            startPoints++;
        }
        if (q4) {
            startPoints++;
        }
        if (q5) {
            startPoints++;
        }
        if (q6a && q6b && !q6c) {
            startPoints++;
        }

        return startPoints;
    }

    private String createPointsSummary(int score) {
        String pointsMessage = "Your score is " + score + " points";
        return pointsMessage;
    }

    private String createScoreMessage(boolean q1, boolean q2, boolean q3, boolean q4, boolean q5, boolean q6a, boolean q6b, boolean q6c) {
        String scoreMessage;
        if (q1) {
            scoreMessage = "Your answer to question 1 is correct";
        } else {
            scoreMessage = "Your answer to question 1 is incorrect";
        }

        if (q2) {
            scoreMessage += "\nYour answer to question 2 is correct";
        } else {
            scoreMessage += "\nYour answer to question 2 is incorrect.Good answer is: dolphin.";
        }

        if (q3) {
            scoreMessage += "\nYour answer to question 3 is correct";
        } else {
            scoreMessage += "\nYour answer to question 3 is incorrect";
        }

        if (q4) {
            scoreMessage += "\nYour answer to question 4 is correct";
        } else {
            scoreMessage += "\nYour answer to question 4 is incorrect";
        }

        if (q5) {
            scoreMessage += "\nYour answer to question 5 is correct";
        } else {
            scoreMessage += "\nYour answer to question 5 is incorrect.Good answer is: giraffe.";
        }
        scoreMessage += "\nQuestion 6: ";

        if (q6a) {
            scoreMessage += "\nYes, it is true that elephants have a very good memory";
        } else {
            scoreMessage += "\nYou did not mark that elephants have a good memory";
        }

        if (q6b) {
            scoreMessage += "\nYes, it is true that after birth a small elephant weighs about 100kg";
        } else {
            scoreMessage += "\nYou did not mark that after birth a small elephant weighs about 100kg";
        }

        if (q6c) {
            scoreMessage += "\nYou made a mistake this time. Elephants ARE mammals.";
        } else {
            scoreMessage += "\nYou were right not to mark this point because elephants ARE mammal.";
        }
        return scoreMessage;
    }
}
